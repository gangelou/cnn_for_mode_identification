import numpy as np
import os
import tensorflow as tf
import pandas as pd
from keras.models import load_model
from pathlib import Path
from keras.preprocessing.image import ImageDataGenerator

###A more intelligent Feeder for the CNN
### All validation is done here.

class predictCNN(object):
   def __init__(self):
 
      self.labels={ 0: 'Spurious modes',
                    1: 'Rosette g modes',
                    2: 'Whispering gallery p modes' ,
                    3: 'G modes with some envelope extent',
                    4: 'Island modes of period 2',
                    5: 'Other p modes'} 


      self.params= {'outputdir':'./outputtest/',
                    'basedir': './Eval_data',
                    'feeddir': 'Gio2z' ,
                    'which_subfolders': ['2Zc6'],   #e.g., './' or '2Zc6' 
                    'model': 'logs/CNNmodel.h5',
                    'image_size': 128,
                    'color_mode' : 'grayscale',}

      self.run_classifier(self.params, self.labels)
       

   def run_classifier(self,params,labels):   
      
      ### Load the model
      model = load_model(params['model'])
      #model=load_model('./logs/hparam_tuning/run-0.mdl_wts.hdf5')
      
      ### Outdir exists? 
      outdir=Path(params['outputdir'])
      if not outdir.is_dir():
         outdir.mkdir()
         
      
      for f in params['which_subfolders']:
          ### This is the folder for a given project
          folder=Path(params['basedir']).joinpath(params['feeddir'],f)
          
          ### But there might be subfolders in the project
          subfolders = [x for x in folder.iterdir() if x.is_dir()]
          
          if len(subfolders) == 0:
              subfolders=[folder]
              
          ### process each subfolder
          for d in subfolders:
             outpath=outdir.joinpath(params['feeddir'])
             if not outpath.is_dir():
                 outpath.mkdir()
                 
             outfile=outpath.joinpath(d.name+'-results.dat')
             
             
             images=list(d.glob('**/*.png'))+list(d.glob('**/*.jpg'))
             cols=['filename', 'catagory', 'label']+ ['Pr-'+str(x) for x in range(len(labels))]
             df=pd.DataFrame(0, index=range(len(images)), columns=cols)
             df['filename']=[x.name for x in sorted(images)]   
             
             datagen=ImageDataGenerator(rescale=1./255)
             generator = datagen.flow_from_dataframe(
                 df,
                 directory=images[0].parent,
                 x_col='filename',
                 color_mode=params['color_mode'],
                 target_size=(params['image_size'], params['image_size']),
                 batch_size=32,
                 class_mode=None,  # only data, no labels
                 shuffle=False)

             soft= model.predict_generator(generator)       
             hard= np.argmax(soft, axis=-1)
             for i, f in enumerate(df.filename):
                 res=(f, hard[i], labels[hard[i]])+tuple(soft[i])
                 df.iloc[i]=res     
             df.to_csv(outfile, columns=cols, index=False)

### 
feed=predictCNN()
