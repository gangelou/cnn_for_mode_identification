import tensorflow as tf
from tensorflow import keras
from keras.layers import Conv2D, MaxPooling2D

from keras.models import Model
from keras.layers import Input
from keras.layers import Conv2D
from keras.layers import Dense, Flatten, Reshape, Dropout
from keras.utils import plot_model
from keras.callbacks import TensorBoard
from tensorboard.plugins.hparams import api as hp
from keras.preprocessing.image import ImageDataGenerator
from gapcv.vision import Images
from pathlib import Path
from keras.applications.inception_resnet_v2 import InceptionResNetV2 as IRV2
from keras.callbacks.callbacks import ModelCheckpoint
import numpy as np
import math
from keras.models import load_model



"""George Angelou's fancy CNN code built for the purpose of analysing rapidly rotating delta scuti stars.
   New version developed at MPA October 2019. This uses Keras rather than TF.estimator and TF.experiment.
   People said the gapcv is the best server/feeder for ML. I had to modify the library manually to get it 
   to work. Ask me for a version if you use that instead of Keras flow from directory etc.."""

 
class CNN(object):
   def __init__(self,optimize):
      hparams,  params=self.set_hparams()
      self.run_experiment(hparams, params, optimize)
      print('Best model trained: logs/CNNmodel.h5')

   def set_hparams(self):  
        """Set all the paramters. Put everything in hyperparamter dictionary for fine tuning if so desired"""
        hparams={
            'optimizer':hp.HParam('optimizer', hp.Discrete(['adam', 'sgd'])), 
            'architecture': hp.HParam('architecture', hp.Discrete(['GCA17'])),
            #'architecture': hp.HParam('architecture', hp.Discrete(['vgg16', 'GCA17', 'IRV2']))}
            'dropout':hp.HParam('dropout', hp.RealInterval(0.1, 0.2)),
            'num_units':hp.HParam('num_units', hp.Discrete([32, 64, 128])), #only for custom architecture
            }


        params= {  
           'train_path': '/afs/mpa/project/ml2/GM/Modes/qtrain/',
           'dataset_name': 'qtrain',
           'log_dir': 'logs/hparam_tuning',
           'test_frac':0.15,
           'image_size': 128,
           'color_mode' : 'grayscale',
           'seed': 0,
           'batch_size': 32,
           'epochs': 500,
           'take_best_epoch':True,
           'feeder':'keras' } #gapcv, keras  
       
        return hparams, params
            

   def run_experiment(self, hparams, params, optimize):
       """Set up and run the experiment now
          hparams: are the ranges set in set_hparams
          hparms:  is what we send runner for a given combination of hparams as we cycle th  """

       ### Write a log file
       METRIC_ACCURACY= 'acc'
       with tf.summary.create_file_writer(params['log_dir']).as_default():
            hp.hparams_config(
                     hparams=[hparams['num_units'],hparams['dropout'],hparams['optimizer'],hparams['architecture'] ],
                     metrics=[hp.Metric(METRIC_ACCURACY, display_name='Acc')],
                      )  
      

       session_num = 0

       #for num_units in hparams['num_units'].domain.values:  #Dont need this if not building a custom architecture
       for dropout_rate in (hparams['dropout'].domain.min_value, hparams['dropout'].domain.max_value):
             for optimizer in hparams['optimizer'].domain.values:
               for architecture in hparams['architecture'].domain.values: 
                 hparms_comb = {
                        hparams['optimizer']: optimizer,
                        hparams['architecture']: architecture,
                        hparams['dropout']: dropout_rate,
                        #hparams['num_units']: num_units,  #for custom architectures!
                          }
                    
                 run_name = "run-%d" % session_num
                 print('--- Starting trial: %s' % run_name)
                 print({h.name: hparms_comb[h] for h in hparms_comb})

                 writepath=params['log_dir']+'/'+run_name 
                 accuracy=self.train_test_model(params,hparams, hparms_comb, writepath)
                 session_num += 1

                 if optimize is False:
                   if session_num > 0:
                      print('Not Optimizing hyperparameters, taking first entry from hparams options')
                      return 



   
   def keras_gen(self,params):
       """Use Keras data feeder"""

       p=Path(params['train_path'])
       n_images=len(list(p.glob('**/*.jpg')))+len(list(p.glob('**/*.png')))        
       n_test = math.floor(n_images*params['test_frac'])
       n_train =   n_images- n_test

       ### -1 to discount parent directory 
       dirs = p.glob('**/')
       n_classes = sum(1 for dir in dirs)
       n_classes -= 1        
  

       print('Aquiring training data from:', params['train_path'])
       print('len images:',n_images)
       print('len train:', n_train)
       print('len test:', n_test)
       print('classes:',  n_classes) 
       params['n_classes']=n_classes


       data_generator = ImageDataGenerator(rescale=1./255, validation_split=params['test_frac'])

       self.train_data = data_generator.flow_from_directory(params['train_path'], target_size=(params['image_size'], params['image_size']), shuffle=True, seed=params['seed'],
                                                      class_mode='categorical', color_mode=params['color_mode'], batch_size=params['batch_size'], subset="training")

       self.val_data = data_generator.flow_from_directory(params['train_path'], target_size=(params['image_size'], params['image_size']), shuffle=True, seed=params['seed'], 
                                                     class_mode='categorical', color_mode=params['color_mode'], batch_size=params['batch_size'], subset="validation")
       self.spe=n_train // params['batch_size']
       self.useMP=False
 
   def gapcv_gen(self,params):
       """Use gapcv data feeder"""
       ### Get the data generators up and running
       data_set_name=Path(params['train_path']).joinpath(params['dataset_name']+'.h5')
       
       ### Doesnt seem to work the first time through. 
       if not data_set_name.is_file():
          images = Images( params['train_path']+'/'+params['dataset_name'], params['train_path'], 
                          config=[params['color_mode'], 'store','stream','norm=pos'])
       

       images = Images(config=['stream'])
       images._dir='/'
       images.load(params['train_path']+'/'+params['dataset_name'])    

       # split data set
       images.split = params['test_frac'],params['seed']
       X_test, Y_test = images.test
       a=X_test.shape
       X_test=X_test.reshape((a[0],a[1],a[2],1))
       print(X_test.shape, Y_test.shape)
       
       self.val_data=(X_test, Y_test) 
       # generator
       images.minibatch = params['batch_size']
       self.train_data = images.minibatch

       total_train_images = images.count - len(X_test)
       n_classes = len(images.classes)
       params['n_classes']=n_classes


       print('Aquiring training data from:', params['train_path'])
       print('len images:',images.count)
       print('len train:', total_train_images)
       print('len test:', len(X_test))
       print('classes:',  n_classes) 

       self.spe=total_train_images // params['batch_size']
       self.useMP=True 


   def train_test_model(self, params,hparams, hcomb, writepath):
       """gapcv is annoying as hell but fast. 
          Need to provide _dir='/' if you give an absolute path to params['train_path'].
          If you stuggle to get this to work set the data_set_name = 'dog_cats'.  (i.e. so it writes to the cwd)
          It will write out a h5 file to the current directory. Then if you want to load it in later give 'dogs_cats' to images.load 
          images._dir=./ can then be commented out """
 

       if params['feeder'] =='gapcv':
           self.gapcv_gen(params)
       elif params['feeder'] =='keras':
           self.keras_gen(params)

       self.best_score= 0.0   

       ### Checkpoints 
       mcp_save = ModelCheckpoint(writepath+'.mdl_wts.hdf5', 
                                  save_best_only=True,
                                  monitor='val_loss',
                                  mode='min')

       tboard=TensorBoard(log_dir=writepath)
       hparmscb= hp.KerasCallback(writepath, hcomb) 

       #images=next(iter(gap_generator))       
       model= self.construct_model(params, hparams, hcomb)
       model.compile( optimizer=hcomb[hparams['optimizer']],
                     loss='categorical_crossentropy',
                     metrics=['accuracy'],
                      )

       History=model.fit_generator( generator = self.train_data,
                           validation_data = self.val_data,
                           steps_per_epoch =self.spe ,
                           epochs = params['epochs'],
                           use_multiprocessing=self.useMP,
                           callbacks=[mcp_save,tboard,hparmscb]   )
  

       if params['take_best_epoch'] is True:
          idx=np.argmin(History.history['val_loss']) 
          accuracy =History.history['accuracy'][idx]
          model=load_model(writepath+'.mdl_wts.hdf5')

       else:     
          accuracy= History.history['accuracy'][-1]

       if accuracy > self.best_score:
            self.best_score=accuracy
            model.save('logs/CNNmodel.h5')   
       return accuracy
       
       
   def construct_model(self, params, hparams, hcomb):
       
       if params['color_mode'] in ['gray','grayscale']:
           num_chan=1
       else:
           num_chan=3
       
       print(num_chan)

       visible = Input(shape=(params['image_size'], params['image_size'],num_chan))
       if hcomb[hparams['architecture']] =='vgg16':          
           nout1=64
           nout2=128
           nout3=256  

           print ('Constructing vgg16 type architecture')
           
           layer = self.vgg_block(visible, nout1, 2)
           layer = self.vgg_block(layer, nout2, 2)
           layer = self.vgg_block(layer, nout3, 4)
           layer= Flatten()(layer)
           layer = Dense(params['n_classes'], activation='softmax')(layer)

       elif hcomb[hparams['architecture']] =='GCA17':
           nout1=64
           nout2=128
           nout3=256  

           print ('Constructing GCA type architecture')
           layer = self.vgg_block(visible, nout1, 1)
           layer = self.vgg_block(layer, nout2, 1)
           layer = self.vgg_block(layer, nout3, 1)
           layer = Reshape((-1, layer.shape[1]* layer.shape[1] * nout2))(layer)
           layer = Dense(nout3, activation='relu')(layer)
           layer = Dropout(hcomb[hparams['dropout']])(layer)
           layer = Dense(nout3, activation='relu')(layer)
           layer = Dropout(hcomb[hparams['dropout']])(layer)
           layer= Flatten()(layer)
           layer = Dense(params['n_classes'], activation='softmax')(layer)

       elif hcomb[hparams['architecture']] =='IRV2':
          model=IRV2(weights=None, input_tensor=visible, pooling='max', classes= params['n_classes'])
          print(model.summary())
          return model


       model = Model(inputs=visible, outputs=layer)
       # summarize model
       print(model.summary())
       return model


   def vgg_block(self, layer_in, n_filters, n_conv):
      """function for creating a vgg block"""

      # add convolutional layers
      for _ in range(n_conv):
          layer_in = Conv2D(n_filters, (3,3), padding='same', activation='relu')(layer_in) 
      # add max pooling layer
      layer_in = MaxPooling2D((2,2), strides=(2,2))(layer_in)
      return layer_in




### Code Starts down here
if __name__ == "__main__":
    cnn=CNN(False)

    
    

