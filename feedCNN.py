#! /opt/anaconda-3/anaconda3/bin/python3


import numpy as np
import os
import tensorflow as tf
from trainCNN import CNN
import pandas as pd


def GMfiles(estimator):
    
    """Process GM files"""
    global impath
    evald='./Eval_data'
    sampled='Gio2z/'
    joined=os.path.join(evald,sampled)
    testdirs=[os.path.join(joined,x) for x in os.listdir(joined) if os.path.isdir(os.path.join(joined,x))]
    print(testdirs)

    outdir=os.path.join('./output', sampled)
    if not os.path.isdir(outdir):
        os.makedirs(outdir)

    for testdir in testdirs:
       impath=[os.path.join(testdir,x) for x in os.listdir(testdir) if x.endswith('.jpg') or x.endswith('.png')]
       result = estimator.predict(input_fn=test_inputs)
       #print(cnn.logs)
       for i, r in enumerate(result):
           if i==0:
             cols= ['filename', 'catagory', 'label']+['Pr-cat-'+str(i) for i in range(cnn.num_classes)]
             print(testdir,': # files =',len(cnn.tfiles), '# cols=', len(cols))
             d=pd.DataFrame(np.zeros((len(cnn.tfiles), len(cols))))       
             d.columns=cols

           plist=[str(cnn.tfiles[i]), r['class'],  cnn.labels[r['class']]]+list(r['probabilities'])      
           d.ix[i]=plist
       d.sort_values('filename',inplace=True)
       outn=testdir.split('/')[-1]       
       d.to_csv(outdir+outn+'.txt',index=False)
    


def DRfiles(estimator):
    global impath
    evald='Reese/'
    folders=['Mass2.00/']
    outdir='Reese/outputs/Oct10Modes/'
    if not os.path.isdir(outdir):
        os.makedirs(outdir)
    folders=[os.path.join(evald,x) for x in folders]
    failed=[]
    for f in folders:
       roots=[] 
       for root, dirs,files in  os.walk(f):
          if root.endswith('+') or root.endswith('-'):                
             impath=[os.path.join(root,x) for x in os.listdir(root) if x.endswith('_128.png')]
             if len(impath)==0:
                print('WARNING: No files found for', root)
                failed.append(root)
                continue
             result = estimator.predict(input_fn=test_inputs)
             for i, r in enumerate(result):
                if i==0:
                    cols= ['filename', 'category', 'label']+['Pr-cat-'+str(i) for i in range(cnn.num_classes)]
                    print(root,': # files =',len(cnn.tfiles), '# cols=', len(cols))
                    d=pd.DataFrame(np.zeros((len(cnn.tfiles), len(cols))))       
                    d.columns=cols
                plist=[str(cnn.tfiles[i]), r['class'],  cnn.labels[r['class']]]+list(r['probabilities'])      
                d.ix[i]=plist
             d.sort_values('filename',inplace=True)
             outl=root.split('/')[1:]
             outn='_'.join(outl)    
             d.to_csv(os.path.join(outdir,outn+'.txt'),index=False)
    print('Failed directories')
    for f in failed:
        print(f)

# Set default flags for the output directories
FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_string(
    flag_name='saved_model_dir', default_value='./output/modes/iter10/',
    docstring='Output directory for model and training stats.')

#Load the CNN class 
cnn=CNN([],[-1])
#wf=open('output/GM.txt', 'a')
    
def infer(argv=None):
    """Run the inference and print the results to stdout."""
    params = tf.contrib.training.HParams()  # Empty hyperparameters
    # Set the run_config where to load the model from
    run_config = tf.contrib.learn.RunConfig()
    run_config = run_config.replace(model_dir=FLAGS.saved_model_dir)
    # Initialize the estimator and run the prediction
    
    estimator = cnn.get_estimator(run_config, params)
    GMfiles(estimator)
    #DRfiles(estimator)

           
           
def test_inputs():
    """Returns training set as Operations.
    Returns:
        (features, ) Operations that iterate over the test set.
    """

    
    with tf.name_scope('Test_data'):
        images,cnn.tfiles = load_images(impath)
        dataset = tf.data.Dataset.from_tensor_slices((images,))

        # Return as iteration in batches of 1
        return dataset.batch(1).make_one_shot_iterator().get_next()


def load_images(impath):
    """Load sample images from directory return them in an array.

    Returns:
        Numpy array of size (10, 28, 28, 1) with MNIST sample images.
    """
    if impath[0].endswith('.jpg'):
        fin=0
    else: 
        fin=1    
    images= cnn.input_pipeline(impath,fin=fin)

    init = tf.global_variables_initializer()
    with tf.Session() as sess:
      sess.run(init)
      #Must star Queue runners or you will be waiting a while. 
      coord = tf.train.Coordinator()
      threads = tf.train.start_queue_runners(coord=coord)
      images,keys =sess.run(images)
      coord.request_stop()
      coord.join(threads)
    return images, keys


# Run script ##############################################
if __name__ == "__main__":
    tf.app.run(main=infer)
