#! /opt/anaconda-3/anaconda3/bin/python3


import os
import tensorflow as tf
import sklearn as sk
import pandas as pd
import numpy as np
import pylab as plt
plt.switch_backend('Qt5Agg')
from astropy.io import fits
import multiprocessing as mp
import argparse 
import math 
import random
import shutil

from tensorflow.examples.tutorials.mnist import input_data as mnist_data
from tensorflow.contrib import slim
from tensorflow.contrib.learn import ModeKeys
from tensorflow.contrib.learn import learn_runner
from tensorflow.python.framework import dtypes
from tensorflow.python.framework.ops import convert_to_tensor
from tensorflow.contrib.data import Dataset, Iterator
import collections
tf.logging.set_verbosity(tf.logging.DEBUG)


# Define data loaders #####################################
class IteratorInitializerHook(tf.train.SessionRunHook):
    """Hook to initialise data iterator after Session is created."""

    def __init__(self):
        super(IteratorInitializerHook, self).__init__()
        self.iterator_initializer_func = None

    def after_create_session(self, session, coord):
        """Initialise the iterator after the session has been created."""
        self.iterator_initializer_func(session)


class CNN(object):
   def __init__(self, ldata,val):
      self.ldata=ldata
      self.val=val
      self.logs=[]
      self.boots=1
      self.bssums=[]
      self.labels={ 0: 'Spurious modes',
                    1: 'Rosette g modes',
                    2: 'Whispering gallery p modes' ,
                    3: 'G modes with some envelope extent',
                    4: 'Island modes of period 2',
                    5: 'Other p modes',
                   } 

      if len(self.val) > 0:
         if -1 not in self.val: 
           self.validation_tests(self.val)

      itd=[x for x in os.listdir('./qtrain') if os.path.isdir(os.path.join('./qtrain',x))]       
      self.num_classes=len(itd)
      self.num_channels=1
      self.h=128
      self.w=128
      batch_size=16
      train_frac=0.8
      train_size=int(math.floor(len(ldata)*train_frac))
      train_size= train_size-train_size%batch_size
         
      
      for i in range(self.boots):    
         ### self.data arrays for feeding are set here. I do not return them
         self.set_GM_inputs(ldata,train_size)
         mdir=os.path.join('./output/qtrain/', 'iter'+str(i+10))
         params = tf.contrib.training.HParams(
                 learning_rate=0.002,
                 #learning_rate=1e-4,
                 #learning_rate=0.001,
                 n_classes=self.num_classes,
                 train_steps=5000,
                 min_eval_frequency=100,
                 batchsize=batch_size,
                 model_dir =mdir,
                 keep_checkpoint_max = 10,
                 architecture=self.architecture_spec)
     
      
         if len(self.val) == 0:
            self.run_experiment(params)
            #self.bssums.append(self.bootsum)
         else:
            self.architecture=params.architecture
            
       
   def run_experiment(self, params):
        
         # Set the run_config and the directory to save the model and stats
         run_config = tf.contrib.learn.RunConfig()
         run_config = run_config.replace(model_dir=params.model_dir)


         learn_runner.run(
           experiment_fn=self.experiment_fn,  # First-class function
           run_config=run_config,  # RunConfig
           schedule="train_and_evaluate",  # What to run
           hparams=params  # HParams
                     )

   def experiment_fn(self,run_config, params):
      """Create an experiment to train and evaluate the model.

      Args:
        run_config (RunConfig): Configuration for Estimator run.
        params (HParam): Hyperparameters

      Returns:
        (Experiment) Experiment for training the mnist model.
      """

      # You can change a subset of the run_config properties as
      run_config = run_config.replace(
        save_checkpoints_steps=params.min_eval_frequency)

      # Define the mnist classifier
      estimator = self.get_estimator(run_config, params)

      #Here we split our data into dev and training sets

      #print(dev_data)
      # Setup data loaders

      #mnist = mnist_data.read_data_sets('./mnist_data', one_hot=False)
      train_input_fn, train_input_hook = self.get_train_inputs(
        batch_size=params.batchsize, in_data=self.tr_labels)
      eval_input_fn, eval_input_hook = self.get_test_inputs(
        batch_size=params.batchsize, in_data=self.dev_labels)
      self.architecture=params.architecture


      # Define the experiment
      experiment = tf.contrib.learn.Experiment(
        estimator=estimator,  # Estimator
        train_input_fn=train_input_fn,  # First-class function
        eval_input_fn=eval_input_fn,  # First-class function
        train_steps=params.train_steps,  # Minibatch steps
        min_eval_frequency=params.min_eval_frequency,  # Eval frequency
        train_monitors=[train_input_hook],  # Hooks for training
        eval_hooks=[eval_input_hook],  # Hooks for evaluation
        eval_steps=None  # Use evaluation feeder until its empty
      )
      return experiment


   # Define model ############################################
   def get_estimator(self,run_config, params):
      """Return the model as a Tensorflow Estimator object.

      Args:
         run_config (RunConfig): Configuration for Estimator run.
         params (HParams): hyperparameters.
      """
      return tf.estimator.Estimator(
        model_fn=self.model_fn,  # First-class function
        params=params,  # HParams
        config=run_config  # RunConfig
        )


   def model_fn(self,features, labels, mode, params):
      """Model function used in the estimator.

      Args:
        features (Tensor): Input features to the model.
        labels (Tensor): Labels tensor for training and evaluation.
        mode (ModeKeys): Specifies if training, evaluation or prediction.
        params (HParams): hyperparameters.

      Returns:    
        (EstimatorSpec): Model to be run by Estimator.
      """
      is_training = mode == ModeKeys.TRAIN
      # Define model's architecture
      logits = self.architecture(features, is_training=is_training)
      predictions = {'class': tf.argmax(logits, axis=-1),
                     'probabilities': tf.nn.softmax(logits),
                     'logits':logits} 
                        
      # Loss, training and eval operations are not needed during inference.
      loss = None
      train_op = None
      eval_metric_ops = {}
      
      if mode != ModeKeys.INFER:          
        loss = tf.losses.sparse_softmax_cross_entropy(
            labels=tf.cast(labels, tf.int32),
            logits=logits)
        train_op = self.get_train_op_fn(loss, params)
        eval_metric_ops = self.get_eval_metric_ops(labels, predictions['class'])
        #self.bootsum=eval_metric_ops
      
      return tf.estimator.EstimatorSpec(
        mode=mode,
        predictions=predictions,
        loss=loss,
        train_op=train_op,
        eval_metric_ops=eval_metric_ops
      )

   def get_train_op_fn(self, loss, params):
      """Get the training Op.

      Args:
         loss (Tensor): Scalar Tensor that represents the loss function.
         params (HParams): Hyperparameters (needs to have `learning_rate`)

      Returns:
        Training Op
      """
      return tf.contrib.layers.optimize_loss(
        loss=loss,
        global_step=tf.train.get_global_step(),
        optimizer=tf.train.AdamOptimizer,
        learning_rate=params.learning_rate
      )


   def get_eval_metric_ops(self,labels, predictions):
      
      """Return a dict of the evaluation Ops.

      Args:
        labels (Tensor): Labels tensor for training and evaluation.
        predictions (Tensor): Predictions Tensor.
      Returns:
        Dict of metric results keyed by name.
      """
      return {
        'Accuracy': tf.metrics.accuracy(
            labels=labels,
            predictions=predictions,
            name='accuracy')
      }





   def architecture_spec(self,inputs, is_training, scope='MnistConvNet'):
      """Return the output operation following the network architecture.

      Args:
        inputs (Tensor): Input Tensor
        is_training (bool): True iff in training mode
        scope (str): Name of the scope of the architecture

      Returns:
         Logits output Op for the network.
      """
      
      kernel_size=[3, 3]
      noputs1=64
      noputs2=128
      noputs3=256
      istride=2
      with tf.variable_scope(scope):
        with slim.arg_scope(
                [slim.conv2d, slim.fully_connected],
                weights_initializer=tf.contrib.layers.xavier_initializer()):
            net = slim.conv2d(inputs, noputs1,kernel_size , padding='VALID',
                              scope='conv1')
            net = slim.max_pool2d(net, 2, stride=istride, scope='pool2')
            net = slim.conv2d(net, noputs2, kernel_size, padding='VALID',
                              scope='conv3')
            net = slim.max_pool2d(net, 2, stride=istride, scope='pool4')
            net = tf.reshape(net, [-1, net.shape[1]* net.shape[1] * noputs2])
            net = slim.fully_connected(net, noputs3, scope='fn5')
            net = slim.dropout(net, is_training=is_training,
                               scope='dropout5')
            net = slim.fully_connected(net, noputs3, scope='fn6')
            net = slim.dropout(net, is_training=is_training,
                               scope='dropout6')
            net = slim.fully_connected(net, self.num_classes, scope='output',
                                       activation_fn=None)
        return net


   def architecture1(self,inputs, is_training, scope='MnistConvNet'):
      """Return the output operation following the network architecture.

      Args:
        inputs (Tensor): Input Tensor
        is_training (bool): True iff in training mode
        scope (str): Name of the scope of the architecture

      Returns:
         Logits output Op for the network.
      """
      
      kernel_size=[5, 5]
      noputs1=32
      noputs2=64
      noputs3=128
      istride=2
      with tf.variable_scope(scope):
        with slim.arg_scope(
                [slim.conv2d, slim.fully_connected],
                weights_initializer=tf.contrib.layers.xavier_initializer()):
            net = slim.conv2d(inputs, noputs1,kernel_size , padding='VALID',
                              scope='conv1')
            net = slim.max_pool2d(net, 2, stride=istride, scope='pool2')
            net = slim.conv2d(net, noputs2, kernel_size, padding='VALID',
                              scope='conv3')
            net = slim.max_pool2d(net, 2, stride=istride, scope='pool4')
            net = tf.reshape(net, [-1, net.shape[1]* net.shape[1] * noputs2])
            net = slim.fully_connected(net, noputs3, scope='fn5')
            net = slim.dropout(net, is_training=is_training,
                               scope='dropout5')
            net = slim.fully_connected(net, noputs3, scope='fn6')
            net = slim.dropout(net, is_training=is_training,
                               scope='dropout6')
            net = slim.fully_connected(net, self.num_classes, scope='output',
                                       activation_fn=None)
        return net



   def architecture2(self,inputs, is_training, scope='MnistConvNet'):
      """Return the output operation following the network architecture.

      Args:
        inputs (Tensor): Input Tensor
        is_training (bool): True iff in training mode
        scope (str): Name of the scope of the architecture

      Returns:
         Logits output Op for the network.
      """
      with tf.variable_scope(scope):
        with slim.arg_scope(
                [slim.conv2d, slim.fully_connected],
                weights_initializer=tf.contrib.layers.xavier_initializer()):
            net = slim.conv2d(inputs, 20, [5, 5], padding='VALID',
                              scope='conv1')
            net = slim.max_pool2d(net, 2, stride=2, scope='pool2')
            net = slim.conv2d(net, 40, [5, 5], padding='VALID',
                              scope='conv3')
            net = slim.max_pool2d(net, 2, stride=2, scope='pool4')
            net = tf.reshape(net, [-1, 4* 4 * 40])
            net = slim.fully_connected(net, 256, scope='fn5')
            net = slim.dropout(net, is_training=is_training,
                               scope='dropout5')
            net = slim.fully_connected(net, 256, scope='fn6')
            net = slim.dropout(net, is_training=is_training,
                               scope='dropout6')
            net = slim.fully_connected(net, 10, scope='output',
                                       activation_fn=None)
        return net


   def architecture3(self,inputs, is_training, scope='MnistConvNet'):
      """Return the output operation following the network architecture.

      Args:
        inputs (Tensor): Input Tensor
        is_training (bool): True iff in training mode
        scope (str): Name of the scope of the architecture

      Returns:
         Logits output Op for the network.
      """
      
      kernel_size=[3, 3]
      noputs1=32
      noputs2=64
      noputs3=128
      istride=2
      with tf.variable_scope(scope):
        with slim.arg_scope(
                [slim.conv2d, slim.fully_connected],
                weights_initializer=tf.contrib.layers.xavier_initializer()):
            net = slim.conv2d(inputs, noputs1,kernel_size , padding='SAME',
                              scope='conv1')
            net = slim.max_pool2d(net, 2, stride=istride, scope='pool2')
            net = slim.conv2d(net, noputs1, kernel_size, padding='SAME',
                              scope='conv3')
            net = slim.max_pool2d(net, 2, istride, scope='pool3a')
            net = slim.conv2d(net, noputs2, kernel_size, padding='SAME',
                              scope='conv3a')
            net = slim.max_pool2d(net, 2, stride=istride, scope='pool4')
            layer_shape = net.get_shape()
            num_features = layer_shape[1:4].num_elements()
            net = tf.reshape(net, [-1, num_features])
            net = slim.fully_connected(net, noputs3, scope='fn5')
            #net = slim.dropout(net, is_training=is_training,
            #                   scope='dropout5')
            #net = slim.fully_connected(net, noputs3, scope='fn6')
            #net = slim.dropout(net, is_training=is_training,
            #                   scope='dropout6')
            net = slim.fully_connected(net, self.num_classes, scope='output',
                                       activation_fn=None)
        return net


   def architecture4(self,inputs, is_training, scope='MnistConvNet'):
      """Return the output operation following the network architecture.

      Args:
        inputs (Tensor): Input Tensor
        is_training (bool): True iff in training mode
        scope (str): Name of the scope of the architecture

      Returns:
         Logits output Op for the network.
      """
      filter_size_conv1 = 3 
      num_filters_conv1 = 32

      filter_size_conv2 = 3
      num_filters_conv2 = 32

      filter_size_conv3 = 3
      num_filters_conv3 = 64
    
      fc_layer_size = 128
      
      
      
      def create_weights(shape):
        return tf.Variable(tf.truncated_normal(shape, stddev=0.05))

      def create_biases(size):
        return tf.Variable(tf.constant(0.05, shape=[size]))
        
        
      def create_convolutional_layer(input,
               num_input_channels, 
               conv_filter_size,        
               num_filters):  
    

         ## We shall define the weights that will be trained using create_weights function.
         weights = create_weights(shape=[conv_filter_size, conv_filter_size, num_input_channels, num_filters])
         ## We create biases using the create_biases function. These are also trained.
         biases = create_biases(num_filters)

         ## Creating the convolutional layer
         layer = tf.nn.conv2d(input=input,
                     filter=weights,
                     strides=[1, 1, 1, 1],
                     padding='SAME')

         layer += biases

         ## We shall be using max-pooling.  
         layer = tf.nn.max_pool(value=layer,
                            ksize=[1, 2, 2, 1],
                            strides=[1, 2, 2, 1],
                            padding='SAME')
         ## Output of pooling is fed to Relu which is the activation function for us.
         layer = tf.nn.relu(layer)

         return layer
  

      def create_flatten_layer(layer):
         #We know that the shape of the layer will be [batch_size img_size img_size num_channels] 
         # But let's get it from the previous layer.
         layer_shape = layer.get_shape()

         ## Number of features will be img_height * img_width* num_channels. But we shall calculate it in place of hard-coding it.
         num_features = layer_shape[1:4].num_elements()

         ## Now, we Flatten the layer so we shall have to reshape to num_features
         layer = tf.reshape(layer, [-1, num_features])

         return layer


      def create_fc_layer(input,          
             num_inputs,    
             num_outputs,
             use_relu=True):
    
         #Let's define trainable weights and biases.
         weights = create_weights(shape=[num_inputs, num_outputs])
         biases = create_biases(num_outputs)

         # Fully connected layer takes input x and produces wx+b.Since, these are matrices, we use matmul function in Tensorflow
         layer = tf.matmul(input, weights) + biases
         if use_relu:
           layer = tf.nn.relu(layer)

         return layer
   
   
   
      net = create_convolutional_layer(input=inputs,
               num_input_channels=self.num_channels,
               conv_filter_size=filter_size_conv1,
               num_filters=num_filters_conv1)
      net = create_convolutional_layer(input=net,
               num_input_channels=num_filters_conv1,
               conv_filter_size=filter_size_conv2,
               num_filters=num_filters_conv2)

      net= create_convolutional_layer(input=net,
               num_input_channels=num_filters_conv2,
               conv_filter_size=filter_size_conv3,
               num_filters=num_filters_conv3)
          
      net = create_flatten_layer(net)

      net = create_fc_layer(input=net,
                     num_inputs=net.get_shape()[1:4].num_elements(),
                     num_outputs=fc_layer_size,
                     use_relu=True)

      net = create_fc_layer(input=net,
                     num_inputs=fc_layer_size,
                     num_outputs=self.num_classes,
                     use_relu=False) 
   
   
      return net 
   
   # Define the training inputs
   def get_train_inputs(self,batch_size, in_data):
      """Return the input function to get the training data.
      
      Args:
        batch_size (int): Batch size of training iterator that is returned
                          by the input function.
        mnist_data (Object): Object holding the loaded mnist data.

      Returns:
        (Input function, IteratorInitializerHook):
            - Function that returns (features, labels) when called.
            - Hook to initialise input iterator.
      """

      iterator_initializer_hook = IteratorInitializerHook()

   
      def train_inputs():
        """Returns training set as Operations.

        Returns:
            (features, labels) Operations that iterate over the dataset
            on every evaluation
        """
        with tf.name_scope('Training_data') as scope:
            # Get Mnist data
            images = self.tr_images
            labels = self.tr_labels
            # Define placeholders
            images_placeholder = tf.placeholder(
                images.dtype, images.shape)
            labels_placeholder = tf.placeholder(
                labels.dtype, labels.shape)
            # Build dataset iterator
            dataset = tf.data.Dataset.from_tensor_slices(
                (images_placeholder, labels_placeholder))
            dataset = dataset.repeat(None)  # Infinite iterations
            dataset = dataset.shuffle(buffer_size=10000)
            dataset = dataset.batch(batch_size)
            iterator = dataset.make_initializable_iterator()
            next_example, next_label = iterator.get_next()
            # Set runhook to initialize iterator
            iterator_initializer_hook.iterator_initializer_func = \
                lambda sess: sess.run(
                    iterator.initializer,
                    feed_dict={images_placeholder: images,
                               labels_placeholder: labels})
            # Return batched (features, labels)
            return next_example, next_label

      # Return function and hook

      return train_inputs, iterator_initializer_hook


   def get_test_inputs(self,batch_size, in_data):
      """Return the input function to get the test data.

      Args:
        batch_size (int): Batch size of training iterator that is returned
                          by the input function.
        mnist_data (Object): Object holding the loaded mnist data.

      Returns:
        (Input function, IteratorInitializerHook):
            - Function that returns (features, labels) when called.
            - Hook to initialise input iterator.
      """
      iterator_initializer_hook = IteratorInitializerHook()

      def test_inputs():
        """Returns training set as Operations.

        Returns:
            (features, labels) Operations that iterate over the dataset
            on every evaluation
        """
        with tf.name_scope('Test_data'):
            # Get Mnist data
            images = self.dev_images
            labels = self.dev_labels
            # Define placeholders
            images_placeholder = tf.placeholder(
                images.dtype, images.shape)
            labels_placeholder = tf.placeholder(
                labels.dtype, labels.shape)
            # Build dataset iterator
            dataset = tf.data.Dataset.from_tensor_slices(
                (images_placeholder, labels_placeholder))
            dataset = dataset.batch(batch_size)
            iterator = dataset.make_initializable_iterator()
            next_example, next_label = iterator.get_next()
            # Set runhook to initialize iterator
            iterator_initializer_hook.iterator_initializer_func = \
                lambda sess: sess.run(
                    iterator.initializer,
                    feed_dict={images_placeholder: images,
                               labels_placeholder: labels})
            return next_example, next_label

      # Return function and hook
      return test_inputs, iterator_initializer_hook

   def set_GM_inputs(self,ldata,train_size):

              ### Little bit conviluted as it was initially designed to overwirte ldata. 
              ### Not strictly necessary but it works and I will leave it.
              locs=[]
              labs=[]
              for root, subdirs, files in os.walk('./qtrain/'):
                for f in files:
                  labs.append(int(os.path.basename(root)[5:]))
                  locs.append(os.path.join(root,f))
              d={'ifile':locs, 'Status':labs}
              self.ldata=pd.DataFrame(d) 
               
              ### We dont have training and dev sets so we split up our data
              train_frac=0.95
              batch_size=32
              train_size=int(math.floor(len(labs)*train_frac))
              train_size= train_size-train_size%batch_size

              ### Randomly pick indicies 
              idx=np.random.choice(len(self.ldata),train_size, replace=False)
              nidx=[x for x in range(len(self.ldata)) if x not in idx]
              tr_data=self.ldata.iloc[idx,:]
              dev_data=self.ldata.iloc[nidx,:]    
              print('Requested training size:', train_size,)
              print('Len of input data', len(self.ldata)) 
              print('Len training data', len(tr_data)) 
              print('Len dev set', len(dev_data))
              print(nidx[0], nidx[50], nidx[100],nidx[200])
              ### Pass list of paths to input input_pipeline
              ### This should encode and read the images into tensors
              filenames=list(dev_data['ifile'])
              self.dev_images,dev_key_labs =  self.input_pipeline(filenames)
              labels=[x for x in dev_data['Status']]
              self.dev_labels = convert_to_tensor(labels, dtype=dtypes.int32)
            
              ### need to prefetch the data. Can't work out how to evaluate these insitu 
              filenames2=list(tr_data['ifile'])
              self.tr_images,tr_key_labs =  self.input_pipeline(filenames2)
              labels2=[x for x in tr_data['Status']]
              self.tr_labels = convert_to_tensor(labels2, dtype=dtypes.int32)
            
              init = tf.global_variables_initializer()
              with tf.Session() as sess:
                 sess.run(init)
                 #Must star Queue runners or you will be waiting a while. 
                 coord = tf.train.Coordinator()
                 threads = tf.train.start_queue_runners(coord=coord)
                 self.dev_images, dev_key_labs, self.dev_labels =sess.run([self.dev_images,dev_key_labs,self.dev_labels])
                 self.tr_images,tr_key_labs, self.tr_labels =sess.run([self.tr_images,tr_key_labs,self.tr_labels])
                 coord.request_stop()
                 coord.join(threads)
              

              ### Batching doesn't behave how I expect. Order comes back differently???  
              dkics=[int(str(key).split('/')[-2][5:]) for key in dev_key_labs]
              tkics=[int(str(key).split('/')[-2][5:]) for key in tr_key_labs]
      
              self.tr_labels=np.array(tkics)
              self.dev_labels=np.array(dkics) 

      
   def input_pipeline(self,filenames,fin=0):
      filename_queue= tf.train.string_input_producer(filenames)
      if fin ==0:
         images, labels = self.filein(filename_queue)
      else:
         images, labels = self.filein2(filename_queue) 
      im_batch, lab_batch=tf.train.batch([images, labels], batch_size=len(filenames), capacity=len(filenames))
      return (im_batch, lab_batch)

  
   def filein(self, filename_queue):
       reader = tf.WholeFileReader()
       key, record_string = reader.read(filename_queue)
       img  = tf.image.decode_jpeg(record_string ,channels=self.num_channels)
       re_image = tf.image.resize_images(img, [self.h, self.w])
       re_image=tf.scalar_mul(1/255, re_image) 

       return re_image, key

   def filein2(self, filename_queue):
       reader = tf.WholeFileReader()
       key, record_string = reader.read(filename_queue)
       img  = tf.image.decode_png(record_string ,channels=self.num_channels)
       re_image = tf.image.resize_images(img, [self.h, self.w])
       re_image=tf.scalar_mul(1/255, re_image) 
       return re_image, key
   
   
   def validation_tests(self, val):
       for ind in val:
          if ind ==1:
              self.h=28
              self.w=28
              self.num_classes=10
              self.num_channels=1
              
              mnist = mnist_data.read_data_sets('./validation/mnist_data', one_hot=False)
              self.tr_images=mnist.train.images.reshape([-1, self.h, self.w, 1])
              self.tr_labels=mnist.train.labels
              self.dev_images=mnist.test.images.reshape([-1, self.h, self.w, 1])
              self.dev_labels=mnist.test.labels  

              params = tf.contrib.training.HParams(
                 learning_rate=0.002,
                 n_classes=self.num_classes,
                 train_steps=5000,
                 min_eval_frequency=100,
                 batchsize=128,
                 model_dir ='./output/mnist_internal',
                 architecture=self.architecture2)
              self.run_experiment(params)

          if ind ==2:  
              self.h=28
              self.w=28
              self.num_classes=10
              self.num_channels=1

              
              ### Little bit conviluted as it was initially designed to overwirte ldata. 
              ### Not strictly necessary but it works and I will leave it.
              locs=[]
              labs=[]
              for root, subdirs, files in os.walk('./validation/mnist_images/trainingSet'):
                for f in files:
                  labs.append(int(os.path.basename(root)))
                  locs.append(os.path.join(root,f))
              d={'ifile':locs, 'Status':labs}
              self.ldata=pd.DataFrame(d) 

              ### We dont have training and dev sets so we split up our data
              train_frac=0.95
              batch_size=128
              train_size=int(math.floor(len(labs)*train_frac))
              train_size= train_size-train_size%batch_size

              ### Randomly pick indicies 
              idx=np.random.choice(len(self.ldata),train_size, replace=False)
              nidx=[x for x in range(len(self.ldata)) if x not in idx]
              tr_data=self.ldata.iloc[idx,:]
              dev_data=self.ldata.iloc[nidx,:]    
              print('Requested training size:', train_size,)
              print('Len of input data', len(self.ldata)) 
              print('Len training data', len(tr_data)) 
              print('Len dev set', len(dev_data))
      
              ### Pass list of paths to input input_pipeline
              ### This should encode and read the images into tensors
              filenames=list(dev_data['ifile'])
              self.dev_images,dev_key_labs =  self.input_pipeline(filenames)
              labels=[x for x in dev_data['Status']]
              self.dev_labels = convert_to_tensor(labels, dtype=dtypes.int32)
            
              ### need to prefetch the data. Can't work out how to evaluate these insitu 
              filenames2=list(tr_data['ifile'])
              self.tr_images,tr_key_labs =  self.input_pipeline(filenames2)
              labels2=[x for x in tr_data['Status']]
              self.tr_labels = convert_to_tensor(labels2, dtype=dtypes.int32)
            
              init = tf.global_variables_initializer()
              with tf.Session() as sess:
                 sess.run(init)
                 #Must star Queue runners or you will be waiting a while. 
                 coord = tf.train.Coordinator()
                 threads = tf.train.start_queue_runners(coord=coord)
                 self.dev_images, dev_key_labs, self.dev_labels =sess.run([self.dev_images,dev_key_labs,self.dev_labels])
                 self.tr_images,tr_key_labs, self.tr_labels =sess.run([self.tr_images,tr_key_labs,self.tr_labels])
                 coord.request_stop()
                 coord.join(threads)

              ### Batching doesn't behave how I expect. Order comes back differently???  
              dkics=[int(str(key).split('/')[-2]) for key in dev_key_labs]
              tkics=[int(str(key).split('/')[-2]) for key in tr_key_labs]
      
              self.tr_labels=np.array(tkics)
              self.dev_labels=np.array(dkics)
              
              params = tf.contrib.training.HParams(
                 learning_rate=0.002,
                 n_classes=self.num_classes,
                 train_steps=5000,
                 min_eval_frequency=100,
                 batchsize=128,
                 model_dir ='./output/mnist_external',
                 architecture=self.architecture2)
              self.run_experiment(params)

          if ind ==3:
            self.h=128
            self.w=128
            self.num_classes=2
            self.num_channels=3
              
            tloc=[]
            dloc=[]
            troot='./validation/dogs_cats/training_data'
            droot='./validation/dogs_cats/testing_data'
            shutil.rmtree('./output/dogs_cats', ignore_errors=True, onerror=None)
            ### Get the training data 
            for root, subdirs, files in os.walk(troot):
               for f in files:
                 if f.endswith('.jpg'):
                    pet=os.path.basename(root)
                    tloc.append(os.path.join(root,f))
                               
            ### Get the dev data       
            for root, subdirs, files in os.walk(droot):
               for f in files:
                  if f.endswith('.jpg'):
                     pet=os.path.basename(root)
                     dloc.append(os.path.join(root,f))
             
            ### Mix it up a bit          
            random.shuffle(dloc)
            random.shuffle(tloc)
             
            print('Training size = ', len(tloc))
            print('Dev size = ', len(dloc))
             
            self.tr_images, tr_key_labs = self.input_pipeline(tloc)
            self.dev_images,dev_key_labs =  self.input_pipeline(dloc)     
            init = tf.global_variables_initializer()
            with tf.Session() as sess:
                sess.run(init)
                #Must star Queue runners or you will be waiting a while. 
                coord = tf.train.Coordinator()
                threads = tf.train.start_queue_runners(coord=coord)
                self.dev_images, dev_key_labs=sess.run([self.dev_images,dev_key_labs])
                self.tr_images,tr_key_labs=sess.run([self.tr_images,tr_key_labs])
                coord.request_stop()
                coord.join(threads)
                
            self.tr_labels=np.array([0 if str(x).split('/')[-2]=='dogs' else 1 for x in  tr_key_labs])
            self.dev_labels=np.array([0 if str(x).split('/')[-2]=='dogs' else 1 for x in  dev_key_labs])
            print(len(self.tr_images), len(self.tr_labels))
            print(len(self.dev_images), len(self.dev_labels))
            print(self.tr_images.shape)
            print(tr_key_labs[:10], self.tr_labels[:10])
            print(np.array_equal(self.tr_images[0],self.tr_images[1]))
            print(self.dev_images[0], self.dev_images[1])
        
            params = tf.contrib.training.HParams(
                 #learning_rate=1e-4,
                 learning_rate=1e-4,
                 n_classes=self.num_classes,
                 train_steps=5000,
                 min_eval_frequency=100,
                 batchsize=32,
                 model_dir ='./output/dogs_cats',
                 architecture=self.architecture4)
            self.run_experiment(params)
              


### Parse arguments to do stuff
parser = argparse.ArgumentParser(description='Miroh data')
parser.add_argument("-i", "--image",action="store_true", default=False,
                    help="Create images of the spectra for the CNN")

parser.add_argument('-c',"--cpu",nargs='?', action="store",  type=int,default=-1,
                    help="Specify the number of cpus to use")


parser.add_argument('-v', action='append',
                    dest='validation',
                    default=[],
                    help="Validation tests, 1: MNIST internal, 2: MNIST external 3: Dog/Cats") 
args = parser.parse_args()

if args.cpu > 0:
   res=args.cpu
else:
   res = mp.cpu_count()


if len(args.validation) > 0:
   print('Running Validation Tests')
   args.validation=[int(x) for x in args.validation]
   if 1 in args.validation:
      print('Running the internal MNIST validation test (Reading bytes into arrays).  Expected Accuracy 99%')
   if 2 in args.validation:
       print('Running the external MNIST validation test (Reading in and converting images).  Expected Accuracy 99%')
   if 3 in args.validation:
       print('Classifying dog and cat images. This takes a while.') 
       print('I dont think I can get better than 65%. I have seen what looks like better performance') 
       print('But statsitcs on sample sizes might be misleading')

cnn=CNN([],args.validation)




